from random import randint

import bpy

for pas in range(30):
    x, y, z = randint(0, 50), randint(0, 50), randint(0, 50) 
    bpy.ops.mesh.primitive_cube_add(location=(x, y, z))

cube = bpy.data.objects["Cube"]
face = cube.data.polygons[0]
bpy.ops.wm.save_as_mainfile(filepath="generated.blend")


print(face)
texture = bpy.data.textures.new("MP", "IMAGE")
texture.image = bpy.data.images.load("/home/talabardon/projets/capsules/blender animations/face_1.jpeg")
print('toto')
