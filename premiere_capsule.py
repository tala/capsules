#!/usr/bin/python
# -*- coding: utf-8 -*-

from moviepy.editor import CompositeVideoClip, VideoFileClip, concatenate, vfx
from moviepy.video.VideoClip import TextClip

capsule_data = {
    "classe": r"1\up{ère}S1",
    "titre": "Première fonction dérivée",
    "objectifs": [
        "découvrir une fonction dérivée",
        "faire le lien entre les interprétations graphique et numérique de cette fonction dérivée ",
    ],
}
text_list = ["ÉMERICK", "Kermit", "Gonzo", "Fozzie"]
clip_list = []

for text in text_list:
    try:
        txt_clip = vfx.fadein(
            TextClip(text, fontsize=70, color="white").set_duration(3), 2
        )
        clip_list.append(txt_clip)
    except UnicodeEncodeError:
        txt_clip = TextClip("Issue with text", fontsize=70, color="white").set_duration(
            3
        )
        clip_list.append(txt_clip)

final_clip = concatenate(clip_list, method="compose")
final_clip.write_videofile("my_concatenation.mp4", fps=24, codec="mpeg4")

# presentation_titre = TextClip(
#     txt=capsule_data["titre"],
#     filename="titre.mp4",
#     size=None,
#     color="white",
#     bg_color="black",
#     fontsize=100,
#     font="Courier-bold",
# )
#
# presentation_titre.set_duration(2)
#
# screensize = (720, 460)
#
# intro = CompositeVideoClip(
#     [presentation_titre.set_pos("center")], size=screensize, duration=10
# )
#

# my_clip.show() # shows the first frame of the clip
# my_clip.show(10.5) # shows the frame of the clip at t=10.5s
# my_clip.show(10.5, interactive = True)
#
# presentation_titre.show()

#  intro.preview()  # preview with default fps=15
# my_clip.preview(fps=25)
# my_clip.preview(fps=15, audio=False) # don't generate/play the audio.
# my_audio_clip.preview(fps=22000)
